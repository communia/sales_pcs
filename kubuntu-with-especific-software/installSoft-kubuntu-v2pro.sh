#!/bin/bash

sudo su
# Add repos
add-apt-repository -y ppa:kubuntu-ppa/backports 
add-apt-repository -y ppa:nextcloud-devs/client
add-apt-repository -y  ppa:appimagelauncher-team/stable
curl https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/ms-teams stable main" > /etc/apt/sources.list.d/teams.list'
wget https://github.com/jitsi/jitsi-meet-electron/releases/download/v2.4.2/jitsi-meet-x86_64.AppImage
# Install
sudo apt update && sudo apt full-upgrade -y 
sudo apt install -y teams gdebi vim vlc inkscape gimp p7zip-full p7zip-rar rar unrar openjdk-11-jre kubuntu-restricted-extras openssh-server curl chromium-browser libnss3-tools libegl1-mesa libgl1-mesa-glx libxcb-xtest0 libreoffice-l10n-ca libreoffice-l10n-es dolphin-nextcloud virtualbox appimagelauncher

# Choose

# Acrobat outdated
# sudo dpkg --add-architecture i386
# sudo apt update
# sudo apt install -y  libxml2:i386 libcanberra-gtk-module:i386 gtk2-engines-murrine:i386 libatk-adaptor:i386
# wget ftp://ftp.adobe.com/pub/adobe/reader/unix/9.x/9.5.5/enu/AdbeRdr9.5.5-1_i386linux_enu.deb
# sudo dpkg -i AdbeRdr9*_i386linux_enu.deb
# sudo apt install -f
# sudo apt -y autoremove
# sudo apt -y update
# sudo apt -y upgrade
# sudo dpkg -i AdbeRdr9.5.5-1_i386linux_enu.deb

# Masterpdf
# curl https://code-industry.net/get-master-pdf-editor-for-ubuntu/?download
# sudo dpkg -i master-pdf-editor-5.6.80-qt5.x86_64.deb

# Config
echo "JAVA_HOME=\"$(update-alternatives --list java)\"" | sudo tee -a /etc/environment > /dev/null # Configure java VM
source /etc/environment

# dpkg -i install AutoFirma_1_6_5.deb
exit


