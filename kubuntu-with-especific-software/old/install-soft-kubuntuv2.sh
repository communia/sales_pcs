#!/bin/bash

# APT
sudo add-apt-repository -y ppa:kubuntu-ppa/backports 
sudo apt update && sudo apt full-upgrade -y 
sudo apt install -y vim vlc inkscape gimp p7zip-full p7zip-rar rar unrar openjdk-8-jre kubuntu-restricted-extras openssh-server curl chromium-browser libnss3-tools
sudo apt install libegl1-mesa libgl1-mesa-glx libxcb-xtest0 # lib for Zoom
sudo apt install libreoffice-l10n-ca libreoffice-l10n-es  

# Configure Java VM
sudo echo "JAVA_HOME=\"$(update-alternatives --list java)\"" | sudo tee -a /etc/environment > /dev/null
source /etc/environment

#Acrobat outdated
sudo dpkg --add-architecture i386
sudo apt update
sudo apt install -y  libxml2:i386 libcanberra-gtk-module:i386 gtk2-engines-murrine:i386 libatk-adaptor:i386
wget ftp://ftp.adobe.com/pub/adobe/reader/unix/9.x/9.5.5/enu/AdbeRdr9.5.5-1_i386linux_enu.deb
sudo dpkg -i AdbeRdr9*_i386linux_enu.deb
sudo apt install -f
sudo apt -y autoremove
sudo apt -y update
sudo apt -y upgrade
sudo dpkg -i AdbeRdr9.5.5-1_i386linux_enu.deb

#Nextcloud
sudo add-apt-repository -y ppa:nextcloud-devs/client
sudo apt update -y
sudo apt install -y dolphin-nextcloud

#Virtualbox
sudo apt install -y  virtualbox

#Appimage 
sudo add-apt-repository -y  ppa:appimagelauncher-team/stable
sudo apt update -y 
sudo apt install -y  appimagelauncher

# Videoconfs
# ----------
# Jitsi
# [!] Do it manually
# Add icon desktop >> Anar a la img i boto secundari obrir amb appimglnchr
wget https://github.com/jitsi/jitsi-meet-electron/releases/download/v2.4.2/jitsi-meet-x86_64.AppImage
# Teams
     curl https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
     sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/ms-teams stable main" > /etc/apt/sources.list.d/teams.list'
     sudo apt -y update
     sudo apt -y install teams
# Zoom
sudo apt -y install gdebi # UX
curl https://zoom.us/client/latest/zoom_amd64.deb
sudo dpkg -i zoom_amd64.deb
# Masterpdf
# curl https://code-industry.net/get-master-pdf-editor-for-ubuntu/?download
# sudo dpkg -i master-pdf-editor-5.6.80-qt5.x86_64.deb



