#!/bin/bash
set -x
#Afegim configuració al skel de tots els ususaris sobre els idimoes del kde
echo -e "[Formats]
LANG=ca_ES.UTF-8

[Translations]
LANGUAGE=ca:es:en_US
" > /etc/skel/.config/plasma-localerc

#Afegim alguna configuració de l'usuari via skel, es podria tirar comandes via /etc/guest-session/prefs.sh, per autostarts: /etc/guest-session/auto.sh
mkdir /etc/guest-session
cp -pr /etc/skel /etc/guest-session/skel

#Afegim configuració al skel del invitat per desactivar el bloqueig de plantalla automàtic
echo -e "[\$Version]
update_info=kscreenlocker.upd:0.1-autolock

[Daemon]
Autolock=false
LockOnResume=false
" > /etc/guest-session/skel/.config/kscreenlockerrc

#Afegim configuració al skel de tots els ususaris sobre el tancament de la sessió
echo -e "[General]
confirmLogout=true
excludeApps=
loginMode=default
offerShutdown=true
shutdownType=0
"> /etc/skel/.config/ksmserverrc

# Movem script de guest-account al bin
cp guest-account /usr/local/bin/
chmod 700 /usr/local/bin/guest-account

mkdir -p /usr/local/lib/guest-account/
cp guest-session-auto.sh /usr/local/lib/guest-account/guest-session-auto.sh
chmod 755 /usr/local/lib/guest-account/guest-session-auto.sh

# Fem que s'executi guest-account cada cop que sengegui sddm
echo -e "
# Try remove the guest account
echo \"Try removing guest account\" | systemd-cat -t guest-account -p info
/usr/local/bin/guest-account remove guest 2>&1 > /dev/null | systemd-cat -t guest-account -p info

# Add the guest account
echo \"adding new guest account\" | systemd-cat -t guest-account -p info
/usr/local/bin/guest-account add 2>&1 > /dev/null | systemd-cat -t guest-account -p info
" >> /usr/share/sddm/scripts/Xsetup

# Fem que s'executi guest-account cada cop que es pari sddm
echo -e "
# Removes first and then re-add the guest account
echo \"removing guest account\" | systemd-cat -t guest-account -p info
/usr/local/bin/guest-account remove guest 2>&1 > /dev/null | systemd-cat -t guest-account -p info
" >> /usr/share/sddm/scripts/Xstop

# Marquem autologin per l'usuari guest a configuracio de sddm
echo -e "
[Autologin]
User=guest
Session=plasma.desktop

[Users]
# Minimum user id for displayed users
MinimumUid=900 
# Guest must between 900 and 999 as it comes from adduser --system that is 1000--, for example 
HideShells=/usr/sbin/nologin,/usr/bin/nologin,/sbin/nologin,/bin/false,/usr/bin/git-shell
# As these are not used by usable guest users. As example systemd-coredump is 999 and uses /usr/bin/nologin and must be excluded from login screen
" >> /etc/sddm.conf

