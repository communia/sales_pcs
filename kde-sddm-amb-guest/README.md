# KDE ( kubuntu ) amb guest
Scripts per tal de posar a punt sales d'ordinadors bàsiques amb kubuntu i una sessió de convidat. Executar amb root o sudo.

Per instal·lar repos i paquets útils extres.

    # install-extra-packages.sh
    
Per agafar informació bàsica del pc i guardar-lo al fitxer inventari.txt en aquesta mateixa carpeta:

    # info-inventari.sh
Per afegir sessió de convidat i configurar kde perquè no salti la pantalla de bloqueig i perquè comenci una sessió en blanc:

    # add-sddm-guest.sh

Scripts que NO S'HA d'executar: 
  - sript de creació de convidat: `# guest-account`


# Install ràpid


```
mkdir /tmp/sales_pcs
cd /tmp/sales_pcs
wget https://gitlab.com/communia/sales_pcs/-/raw/master/kde-sddm-amb-guest/guest-account
wget https://gitlab.com/communia/sales_pcs/-/raw/master/kde-sddm-amb-guest/guest-session-auto.sh
sudo -v && wget -nv -O- https://gitlab.com/communia/sales_pcs/-/raw/master/kde-sddm-amb-guest/install-extra-packages.sh | sudo sh /dev/stdin
sudo -v && wget -nv -O- https://gitlab.com/communia/sales_pcs/-/raw/master/kde-sddm-amb-guest/add-sddm-guest.sh | sudo sh /dev/stdin
```





# Troubleshooting
Si veiem que queda sessió en pantalla negre, pot ser que en algun moment s'hagi apagat l'ordinador a mitges entre la creació de l'usuari i de la creació del home, veure a `/etc/passwd` si existeix l'usuari guest. Si hi és fer un `userdel guest` i reiniciar.
