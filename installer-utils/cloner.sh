#In case you want to operate with a larger Block Size than the standard disk block size (512), let's say up to four times the size of standard block size (2048), for speeding up things, you can do some simple math and multiply by four the standard block size of the disk (512x4) and then divide the total number of blocks +1 of the last partition by four.
# set here the default input file of dcfldd
IF_dcfldd=sda
# set here the final partition that's determining the frontier of what will be damp
#(as example 1 means sda1, 2 means sda2,...)
IF_ENDING_PARTITION=2 #computed later
# set here the defaultoutput partitions:
OFS_dcfldd="sdb sdc"
# set here how much you want to multiply the block size by default:
BS_MULTIPLIER_dcfldd=4
# Set the prefix of the hostname to be applied to new systems
HOSTNAME_PREFIX="SJ-"
# set the start of the series that will be appended to the hostname in new systems
HOSTNAME_START=1
# set the decimals of the series that will be appended to the hostname in new systems
HOSTNAME_DECIMALS=3


echo "List of devices currently in the system:"
lsblk -o NAME,MAJ:MIN,SIZE,TYPE,MOUNTPOINT,LABEL,VENDOR,MODEL,REV,SERIAL
echo ""

## Interactive start
read -e -i "$IF_dcfldd" -p "Please set the origin device:[$IF_dcfldd] " input
IF_dcfldd="${input:-$IF_dcfldd}"
read -e -i "$OFS_dcfldd" -p "Please set the target devices where ${IF_dcfldd} will be cloned:[${OFS_dcfldd}] " input
OFS_dcfldd=( ${input:-$OFS_dcfldd} )

IF_ENDING_PARTITION=$(ls /sys/block/${IF_dcfldd}/*/partition | wc -l)
read -e -i "${IF_ENDING_PARTITION}" -p "Please set the last partition number that will be cloned(previous ones will be cloned too):[${IF_ENDING_PARTITION}] " input
IF_ENDING_PARTITION="${input:-$IF_ENDING_PARTITION}"

read -e -i "${BS_MULTIPLIER_dcfldd}" -p "Please set how much you want to multiply the block size to speed up dump:[${BS_MULTIPLIER_dcfldd}] " input
BS_MULTIPLIER_dcfldd="${input:-$BS_MULTIPLIER_dcfldd}"

HOSTNAME_PREFIX="SJ-"
read -e -i "${HOSTNAME_PREFIX}" -p "Please set the prefix of the hostname to be applied to new systems:[${HOSTNAME_PREFIX}] " input
HOSTNAME_NAME="${input:-$HOSTNAME_PREFIX}"

HOSTNAME_START=1
read -e -i "${HOSTNAME_START}" -p "Please set the start of the series that will be appended to the hostname in new systems:[${HOSTNAME_START}] " input
HOSTNAME_START="${input:-$HOSTNAME_START}"

HOSTNAME_DECIMALS=3
read -e -i "${HOSTNAME_DECIMALS}" -p "Please set the decimals of the series that will be appended to the hostname in new systems:[${HOSTNAME_DECIMALS}] " input
HOSTNAME_DECIMALS="${input:-$HOSTNAME_DECIMALS}"


## End of interactive


# do not touch that as it's logic:
OFS_DD_imploded=( "${OFS_dcfldd[@]/#/of=/dev/}" )
IF_SECTOR_SIZE_dcfldd=$(cat /sys/block/${IF_dcfldd}/queue/hw_sector_size)
BS_dcfldd=$(expr $BS_MULTIPLIER_dcfldd \* $IF_SECTOR_SIZE_dcfldd)
let COUNT_dcfldd=($(cat /sys/block/${IF_dcfldd}/${IF_dcfldd}${IF_ENDING_PARTITION}/start)+$(cat /sys/block/${IF_dcfldd}/${IF_dcfldd}${IF_ENDING_PARTITION}/size))/${BS_MULTIPLIER_dcfldd}

#Please install deps:
echo "First of all install deps:
$(tput bold 1)sudo apt install cloud-guest-utils dcfldd$(tput sgr 0)
"

# we echo the command to be less dangerous as it's risky
echo "$(tput bold 1)To clone /dev/$IF_dcfldd to devices ${OFS_dcfldd[@]} using block size of ${BS_dcfldd} , ${BS_MULTIPLIER_dcfldd}x the block size of /dev/$IF_dcfldd ($IF_SECTOR_SIZE_dcfldd):$(tput sgr 0)
"
echo "dcfldd if=/dev/${IF_dcfldd} bs=${BS_dcfldd} count=${COUNT_dcfldd}  conv=sync,noerror ${OFS_DD_imploded[@]}
"
echo "$(tput bold 1)To check if can be expanded after:$(tput sgr 0)
"
for device in "${OFS_dcfldd[@]}"; do echo "growpart --dry-run /dev/$device $IF_ENDING_PARTITION" ; done

echo "
$(tput bold 1)When sure run:$(tput sgr 0)
"

for device in "${OFS_dcfldd[@]}"; do echo "growpart /dev/$device $IF_ENDING_PARTITION" ; done

echo "
$(tput bold 1)To proceed with must-of check filesystem if it's ext run:$(tput sgr 0)
"

for device in "${OFS_dcfldd[@]}"; do echo "e2fsck -f /dev/$device$IF_ENDING_PARTITION" ; done

echo "
$(tput bold 1)To extend filesystem if it's ext run:$(tput sgr 0)
"
for device in "${OFS_dcfldd[@]}"; do echo "resize2fs /dev/$device$IF_ENDING_PARTITION" ; done

echo "
$(tput bold 1)To set hostname run:$(tput sgr 0)
"

mkdir /media/cloner-dd/
mount /dev/${IF_dcfldd}${IF_ENDING_PARTITION} /media/cloner-dd
ORIGIN_HOSTNAME=$(cat /media/cloner-dd/etc/hostname)
umount /media/cloner-dd

for device in "${OFS_dcfldd[@]}"; 
do
  hostname_serial=$(printf "%0${HOSTNAME_DECIMALS}d" $HOSTNAME_START )
  newhost=${HOSTNAME_PREFIX}${hostname_serial}
  echo "e2label /dev/$device$IF_ENDING_PARTITION ${newhost}"
  echo "mount /dev/$device$IF_ENDING_PARTITION /media/cloner-dd"
  echo "sed -i \"s/$ORIGIN_HOSTNAME/$newhost/g\" /media/cloner-dd/etc/hosts"
  echo "sed -i \"s/$ORIGIN_HOSTNAME/$newhost/g\" /media/cloner-dd/etc/hostname"
  echo "umount /media/cloner-dd"
  (( HOSTNAME_START++ ))
done

echo "
$(tput bold 1)To get the final dump status run:$(tput sgr 0)
"
echo "lsblk -o NAME,MAJ:MIN,SIZE,TYPE,MOUNTPOINT,LABEL,VENDOR,MODEL,REV,SERIAL"

